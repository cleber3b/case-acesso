package br.com.usuario.controller;

import br.com.usuario.models.dto.CriarUsuarioRequest;
import br.com.usuario.models.dto.CriarUsuarioResponse;
import br.com.usuario.models.dto.GetUsuarioResponse;
import br.com.usuario.models.dto.UsuarioMapper;
import br.com.usuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @Autowired
    UsuarioMapper usuarioMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CriarUsuarioResponse cadastrarUsuario(@RequestBody @Valid CriarUsuarioRequest criarUsuarioRequest){
        return usuarioMapper.toCriarUsuarioResponse(usuarioService.cadastrarUsuario(usuarioMapper.toUsuario(criarUsuarioRequest)));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public GetUsuarioResponse buscarUsuarioPorId(@PathVariable int id){
        return usuarioMapper.toGetUsuarioResponse(usuarioService.buscarUsuarioPorId(id));
    }

}
