package br.com.usuario.service;

import br.com.usuario.exception.UsuarioConstraintViolationException;
import br.com.usuario.exception.UsuarioNotFoundException;
import br.com.usuario.models.Usuario;
import br.com.usuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario cadastrarUsuario(Usuario usuario) {

        try {
            return usuarioRepository.save(usuario);
        }catch (RuntimeException e){
            throw new UsuarioConstraintViolationException();
        }
    }

    public Usuario buscarUsuarioPorId(int id) {
        Optional<Usuario> optUsuario = usuarioRepository.findById(id);

        if(!optUsuario.isPresent()){
            throw new UsuarioNotFoundException();
        }

        return optUsuario.get();
    }
}
