package br.com.usuario.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CriarUsuarioRequest {

    @NotNull(message ="Nome precisa ser preenchido.")
    @Size(min = 3, message = "Nome precisa ter no mínimo 3 letras.")
    @NotBlank(message = "Nome precisa ser preenchido.")
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
