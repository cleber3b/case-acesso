package br.com.usuario.models.dto;

import br.com.usuario.models.Usuario;
import org.springframework.stereotype.Component;

@Component
public class UsuarioMapper {

    public Usuario toUsuario(CriarUsuarioRequest criarUsuarioRequest) {
        Usuario usuario = new Usuario();
        usuario.setNome(criarUsuarioRequest.getNome());

        return usuario;
    }

    public CriarUsuarioResponse toCriarUsuarioResponse(Usuario usuario) {
        CriarUsuarioResponse criarUsuarioResponse = new CriarUsuarioResponse();
        criarUsuarioResponse.setId(usuario.getId());
        criarUsuarioResponse.setNome(usuario.getNome());

        return criarUsuarioResponse;
    }

    public GetUsuarioResponse toGetUsuarioResponse(Usuario usuario) {
        GetUsuarioResponse getUsuarioResponse = new GetUsuarioResponse();
        getUsuarioResponse.setId(usuario.getId());
        getUsuarioResponse.setNome(usuario.getNome());

        return getUsuarioResponse;
    }
}
