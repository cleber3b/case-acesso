package br.com.usuario.exception;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class UsuarioConfiguration {

    @Bean
    public ErrorDecoder getClienteDecoder(){
        return new UsuarioDecoder();
    }

}
