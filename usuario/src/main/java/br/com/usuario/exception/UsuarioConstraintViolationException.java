package br.com.usuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Usuário já cadastrado com o nome informado.")
public class UsuarioConstraintViolationException extends RuntimeException {
}
