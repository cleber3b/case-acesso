package br.com.porta.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta já cadastrada para o Andar informado.")
public class PortaException extends RuntimeException {


}
