package br.com.porta.exception;

import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new PortaNotFoundException(); //lança minha exceção
        }
        return errorDecoder.decode(s, response);
        //nesse caso qualquer coisa do erro acima 404 vai assumir como default o erroDecoder do Feign

    }
}
