package br.com.porta.controller;

import br.com.porta.models.dto.CriarPortaRequest;
import br.com.porta.models.dto.CriarPortaResponse;
import br.com.porta.models.dto.GetPortaResponse;
import br.com.porta.models.dto.PortaMapper;
import br.com.porta.service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @Autowired
    PortaMapper portaMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CriarPortaResponse cadastrarUsuario(@RequestBody @Valid CriarPortaRequest criarPortaRequest){
        return portaMapper.toCriarPortaResponse(portaService.cadastrarPorta(portaMapper.toPorta(criarPortaRequest)));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public GetPortaResponse buscarUsuarioPorId(@PathVariable int id){
        return portaMapper.toGetPortaResponse(portaService.buscarPortaPorId(id));
    }

}
