package br.com.porta.repository;

import br.com.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PortaRepository extends CrudRepository<Porta, Integer> {

    Optional<Porta> findByAndarAndSala(String andar, String sala);


}
