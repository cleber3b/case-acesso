package br.com.porta.service;

import br.com.porta.exception.PortaException;
import br.com.porta.exception.PortaNotFoundException;
import br.com.porta.models.Porta;
import br.com.porta.repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    PortaRepository portaRepository;

    public Porta cadastrarPorta(Porta porta) {
        Optional<Porta> optJaExisteAndarSala = portaRepository.findByAndarAndSala(porta.getAndar(), porta.getSala());

        if (optJaExisteAndarSala.isPresent()) {
            throw new PortaException();
        }

        return portaRepository.save(porta);
    }

    public Porta buscarPortaPorId(int id) {
        Optional<Porta> optPorta = portaRepository.findById(id);

        if (!optPorta.isPresent()) {
            throw new PortaNotFoundException();
        }

        return optPorta.get();
    }
}
