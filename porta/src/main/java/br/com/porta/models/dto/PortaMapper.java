package br.com.porta.models.dto;

import br.com.porta.models.Porta;
import org.springframework.stereotype.Component;

@Component
public class PortaMapper {

    public Porta toPorta(CriarPortaRequest criarPortaRequest) {
        Porta porta = new Porta();
        porta.setAndar(criarPortaRequest.getAndar());
        porta.setSala(criarPortaRequest.getSala());

        return porta;
    }

    public CriarPortaResponse toCriarPortaResponse(Porta porta) {
        CriarPortaResponse criarPortaResponse = new CriarPortaResponse();
        criarPortaResponse.setId(porta.getId());
        criarPortaResponse.setAndar(porta.getAndar());
        criarPortaResponse.setSala(porta.getSala());

        return criarPortaResponse;
    }

    public GetPortaResponse toGetPortaResponse(Porta porta) {
        GetPortaResponse getPortaResponse = new GetPortaResponse();
        getPortaResponse.setId(porta.getId());
        getPortaResponse.setAndar(porta.getAndar());
        getPortaResponse.setSala(porta.getSala());

        return getPortaResponse;
    }
}
