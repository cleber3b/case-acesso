package br.com.porta.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CriarPortaRequest {

    @NotNull(message ="Andar precisa ser preenchido.")
    @NotBlank(message = "Andar precisa ser preenchido.")
    private String andar;

    @NotNull(message ="Sala precisa ser preenchido.")
    @NotBlank(message = "Sala precisa ser preenchido.")
    private String sala;

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
