package br.com.logAcesso;

import br.com.logAcesso.producer.models.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class AcessoLogConsumer {

    @KafkaListener(topics = "spec4-cleberson-soares-3", groupId = "cleberson-soares3")
    public void receberAcesso(@Payload Acesso acesso) {

        String msgAviso = "";
        if (acesso.getAcessoPermitido()) {
            msgAviso = "PERMITIDO";
        } else {
            msgAviso = "NEGADO";
        }

        System.out.println("Acesso " + msgAviso + " para o Cliente(" + acesso.getIdUsuario() + ") na Porta(" + acesso.getIdPorta() + ").");

        gerarCSVLog(acesso);
    }

    private static void gerarCSVLog(Acesso acesso) {

        String heading = "Usuario; Cliente; Acesso Permitido";
        FileWriter writer;

        try {
            File arquivoCSV = new File("/home/a2w/Documentos/workspace/PR3/especializacao/case-acesso/logAcessoConsumer/logAcesso.csv");

            if (arquivoCSV.exists()) {
                writer = new FileWriter(arquivoCSV, true);
            } else {
                writer = new FileWriter(arquivoCSV, true);
                writer.write(heading);
            }

            writer.append('\n');
            writer.append(String.valueOf(acesso.getIdUsuario()));
            writer.append(';');
            writer.append(String.valueOf(acesso.getIdPorta()));
            writer.append(';');
            writer.append(acesso.getAcessoPermitido().toString());

            writer.flush();
            writer.close();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
