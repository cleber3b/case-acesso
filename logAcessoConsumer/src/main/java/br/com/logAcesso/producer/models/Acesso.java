package br.com.logAcesso.producer.models;

public class Acesso {

    private int idUsuario;

    private int idPorta;

    private Boolean acessoPermitido;

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdPorta() {
        return idPorta;
    }

    public void setIdPorta(int idPorta) {
        this.idPorta = idPorta;
    }

    public Boolean getAcessoPermitido() {
        return acessoPermitido;
    }

    public void setAcessoPermitido(Boolean acessoPermitido) {
        this.acessoPermitido = acessoPermitido;
    }
}
