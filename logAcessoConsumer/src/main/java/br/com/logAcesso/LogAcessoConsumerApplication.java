package br.com.logAcesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogAcessoConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogAcessoConsumerApplication.class, args);
	}

}
