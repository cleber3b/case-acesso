package br.com.logAcesso.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogAcessoProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogAcessoProducerApplication.class, args);
	}

}
