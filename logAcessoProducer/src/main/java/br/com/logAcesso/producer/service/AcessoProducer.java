package br.com.logAcesso.producer.service;

import br.com.logAcesso.producer.models.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void verificaAcesso(int idUsuario, int idPorta) {

        Acesso acesso = new Acesso();
        acesso.setIdUsuario(idUsuario);
        acesso.setIdPorta(idPorta);
        acesso.setAcessoPermitido(Math.random() < 0.5);

        producer.send("spec4-cleberson-soares-3", acesso);

    }
}
