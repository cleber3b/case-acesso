package br.com.logAcesso.producer.controller;

import br.com.logAcesso.producer.service.AcessoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class AcessoController {

    @Autowired
    AcessoProducer acessoProducer;

    @PostMapping("/acesso/{idUsuario}/{idPorta}")
    public void verificaAcesso(@PathVariable int idUsuario, @PathVariable int idPorta){

        acessoProducer.verificaAcesso(idUsuario, idPorta);

    }
}
