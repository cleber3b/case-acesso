package br.com.acesso.service;

import br.com.acesso.exception.AcessoJaCadastrado;
import br.com.acesso.exception.AcessoNotFoundException;
import br.com.acesso.models.Acesso;
import br.com.acesso.porta.Porta;
import br.com.acesso.porta.PortaAcesso;
import br.com.acesso.porta.PortaNotFoundException;
import br.com.acesso.repository.AcessoRepository;
import br.com.acesso.usuario.Usuario;
import br.com.acesso.usuario.UsuarioAcesso;
import br.com.acesso.usuario.UsuarioNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class AcessoService {

    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    private PortaAcesso portaAcesso;

    @Autowired
    private UsuarioAcesso usuarioAcesso;


    public Acesso salvarAcesso(Acesso acesso) {

        Acesso existeAcesso = acessoRepository.findByIdUsuarioAndIdPorta(acesso.getIdUsuario(), acesso.getIdPorta());
        if(existeAcesso != null) {
            throw new AcessoJaCadastrado();
        }

        //Busca no Microserviço de Usuário
        Usuario usuario = usuarioAcesso.buscarUsuarioPorId(acesso.getIdUsuario());
        if(usuario == null){
            throw new UsuarioNotFoundException();
        }

        //Busca no Microserviço de Porta
        Porta porta = portaAcesso.buscarPortaPorId(acesso.getIdPorta());
        if(porta == null){
            throw new PortaNotFoundException();
        }

        return acessoRepository.save(acesso);
    }

    public Acesso buscarPorIdUsuarioIdPorta(int idUsuario, int idPorta) {

        Acesso acesso = acessoRepository.findByIdUsuarioAndIdPorta(idUsuario, idPorta);

        if(acesso == null){
            throw new AcessoNotFoundException();
        }

        return acesso;
    }

    //Incluido o transaction pois estamos fazendo uma operação arriscada em banco de Dados
    // e ele garante que em caso de erro nada será feito
    @Transactional
    public void deletearAcesso(int idUsuario, int idPorta){

        Acesso acesso = this.buscarPorIdUsuarioIdPorta(idUsuario, idPorta);
        acessoRepository.delete(acesso);
    }


}
