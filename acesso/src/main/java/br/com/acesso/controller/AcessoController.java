package br.com.acesso.controller;

import br.com.acesso.models.Acesso;
import br.com.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso cadastrarUsuario(@RequestBody Acesso acesso){
        return acessoService.salvarAcesso(acesso);
    }

    @GetMapping("/{idUsuario}/{idPorta}")
    @ResponseStatus(HttpStatus.OK)
    public Acesso buscarPorIdUsuarioIdPorta(@PathVariable int idUsuario,@PathVariable int idPorta){
        return acessoService.buscarPorIdUsuarioIdPorta(idUsuario, idPorta);
    }

    @DeleteMapping("/{idUsuario}/{idPorta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletearAcesso(@PathVariable int idUsuario,@PathVariable int idPorta){
        acessoService.deletearAcesso(idUsuario, idPorta);
    }
}

