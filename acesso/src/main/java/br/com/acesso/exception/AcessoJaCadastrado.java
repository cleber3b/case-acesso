package br.com.acesso.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE,reason = "Este acesso já foi cadastrado!" )
public class AcessoJaCadastrado extends RuntimeException {
}
