package br.com.acesso.exception;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;


public class AcessoConfiguration {

    @Bean
    public ErrorDecoder getAcessoDecoder() {

        return new AcessoDecoder();
    }

}
