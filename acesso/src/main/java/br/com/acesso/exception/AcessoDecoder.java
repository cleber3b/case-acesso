package br.com.acesso.exception;

import br.com.acesso.porta.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class AcessoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new AcessoNotFoundException();
        }
        return errorDecoder.decode(s, response);
        //nesse caso qualquer coisa do erro acima 404 vai assumir como default o erroDecoder do Feign

    }
}
