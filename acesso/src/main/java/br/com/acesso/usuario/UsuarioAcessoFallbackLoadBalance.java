package br.com.acesso.usuario;

import com.netflix.client.ClientException;

public class UsuarioAcessoFallbackLoadBalance implements UsuarioAcesso {

    private Exception exception;

    public UsuarioAcessoFallbackLoadBalance(Exception exception){
        this.exception = exception;
    }

    @Override
    public Usuario buscarUsuarioPorId(int id) {
        if(exception.getCause() instanceof ClientException){
            throw new UsuarioNotAvaliable();
        }
        throw (RuntimeException) exception;
    }
}
