package br.com.acesso.usuario;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "MicroServiço de Usuário fora do ar.")
public class UsuarioNotAvaliable extends RuntimeException {
}
