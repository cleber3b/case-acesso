package br.com.acesso.usuario;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "usuario", configuration = UsuarioAcessoConfiguration.class)
public interface UsuarioAcesso {

    @GetMapping("/usuario/{id}")
    Usuario buscarUsuarioPorId(@PathVariable int id);
}
