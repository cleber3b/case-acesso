package br.com.acesso.usuario;

import feign.Response;
import feign.codec.ErrorDecoder;

public class UsuarioAcessoDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new UsuarioNotFoundException();
        }
        return errorDecoder.decode(s, response);
        //nesse caso qualquer coisa do erro acima 404 vai assumir como default o erroDecoder do Feign

    }
}
