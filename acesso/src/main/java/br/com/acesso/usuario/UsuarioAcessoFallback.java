package br.com.acesso.usuario;

public class UsuarioAcessoFallback implements UsuarioAcesso {

    @Override
    public Usuario buscarUsuarioPorId(int id) {
        throw new UsuarioNotAvaliable();
    }
}
