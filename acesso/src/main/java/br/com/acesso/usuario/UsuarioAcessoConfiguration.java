package br.com.acesso.usuario;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class UsuarioAcessoConfiguration {

    @Bean
    public ErrorDecoder getUsuarioAcessoDecoder() {

        return new UsuarioAcessoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new UsuarioAcessoFallback(), RetryableException.class)
                .withFallbackFactory(UsuarioAcessoFallbackLoadBalance::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
