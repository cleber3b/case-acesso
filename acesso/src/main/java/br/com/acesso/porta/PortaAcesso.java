package br.com.acesso.porta;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaAcessoConfiguration.class)
public interface PortaAcesso {

    @GetMapping("/porta/{id}")
    Porta buscarPortaPorId(@PathVariable int id);
}
