package br.com.acesso.porta;

import com.netflix.client.ClientException;

public class PortaAcessoFallbackLoadBalance implements PortaAcesso {

    private Exception exception;

    public PortaAcessoFallbackLoadBalance(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Porta buscarPortaPorId(int id) {
        if (exception.getCause() instanceof ClientException) {
            throw new PortaNotAvaliable();
        }
        throw (RuntimeException) exception;
    }
}
