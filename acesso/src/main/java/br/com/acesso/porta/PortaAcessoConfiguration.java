package br.com.acesso.porta;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;


public class PortaAcessoConfiguration {

    @Bean
    public ErrorDecoder getPortaAcessoDecoder(){
        return new PortaAcessoDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new PortaAcessoFallback(), RetryableException.class)
                .withFallbackFactory(PortaAcessoFallbackLoadBalance::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }


}
