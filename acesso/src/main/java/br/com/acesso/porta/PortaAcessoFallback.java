package br.com.acesso.porta;

public class PortaAcessoFallback implements PortaAcesso {

    /*Aqui apos o implements vc define a regra de negócio e ação que vai acontecer
     * quando o serviço de Cliente for chamado e nõa tiver nenhuma resposta
     * */

    @Override
    public Porta buscarPortaPorId(int id) {
        throw new PortaNotAvaliable();
    }
}
