package br.com.acesso.porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "MicroServiço de Porta fora do ar.")
public class PortaNotAvaliable extends RuntimeException {
}
