package br.com.acesso.repository;

import br.com.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    Acesso findByIdUsuarioAndIdPorta(int idUsuario, int idPorta);

}
